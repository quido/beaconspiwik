# README #

## Assumptions ##

* mobile app only reads data from beacons and is passing it to Piwik plugin
* conditions to send data to Piwik:
    * user device has to be in specified range (below defined distance). Note: distance between device and beacon is calculated basing on RSSI signal strength, so it may be inaccurate and fluctuate betwee 0,5 and 2 meters (or more)
    * AND last in this range for at least x seconds.
* Beacons are assigned to particular areas in Piwik plugin. Sample areas: shop entry, t-shirts, cash desk
* What to measure:
    * path in shop
    * time spent in each area
    * conversions - for eg. user saw the ad in mobile app, went to shop, his path was tracked, so conversion migth be going into area of product and then to cash desk
    * time between displaying ad in mobile app and getting user in range of beacon assigned to promoted product

### What the app is doing? ###

At the moment it scans beacons checks distance to them and if distance is low enough, calls function which is supposed to send data over REST API.

Main file: app/src/main/java/com/example/jaro/beaconstest/BeaconDistance.java

### Contributors ###

* Jarek Miazga