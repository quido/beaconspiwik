package com.example.jaro.beaconstest;

import android.app.Activity;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.widget.TextView;
import android.bluetooth.BluetoothAdapter;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.Utils;

import java.util.List;
import java.util.UUID;

public class BeaconDistance extends Activity {
    private static final String TAG = BeaconDistance.class.getSimpleName();

    /*  All beacons has the same UUID. It can be changed by using SDK, but after that beacons
        won't be visible in Estimote's app anymore. As it might be useful (some beacons settings
        can be easily changed by using it) the assumption is to use Major ID as beacon identifier.
        Proximity UUID is used only for ranging the beacons. */
    private static final String ESTIMOTE_PROXIMITY_UUID = "B9407F30-F5F8-466E-AFF9-25556B57FE6D";
    private static final Region ALL_ESTIMOTE_BEACONS = new Region("regionId", ESTIMOTE_PROXIMITY_UUID, null, null);

    private TextView textView;
    private BeaconManager beaconManager;
    private double MAX_DISTANCE = 0.5;   // in meters. Accuracy of distance is rather low, it can fluctuate between 0.5 and 2 meters  TODO: build UI for setting this

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // ######## Set view ##########
        setContentView(R.layout.activity_beacon_distance);
        textView = new TextView(this);
        textView.setTextSize(20);

        // ######## Beacon Manager ###########
        beaconManager = new BeaconManager(this);
        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override public void onBeaconsDiscovered(Region region, final List<Beacon> rangedBeacons) {
                String message = "Ranged beacons num: " + rangedBeacons.size();

                for (Beacon rangedBeacon : rangedBeacons) {
                    Integer major = rangedBeacon.getMajor();
                    if ( isCloseEnough(rangedBeacon) ) {
                        sendToPiwik(rangedBeacon);
                        message += "\n\nTracked with beacon identified by Major: " + major;
                    }
                }

                textView.setText(message);
                setContentView(textView);
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override public void onServiceReady() {
                try {
                    beaconManager.startRanging(ALL_ESTIMOTE_BEACONS);
                    Log.d(TAG, "Starting ranging");
                    textView.setText("Ranging");
                    setContentView(textView);
                } catch (RemoteException e) {
                    Log.e(TAG, "Cannot start ranging", e);
                }
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            beaconManager.stopRanging(ALL_ESTIMOTE_BEACONS);
        } catch (RemoteException e) {
            Log.e(TAG, "Cannot stop but it does not matter now", e);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        beaconManager.disconnect();
    }

    private boolean isCloseEnough(Beacon beacon) {
        double distance = Utils.computeAccuracy(beacon);
        Log.i(TAG, String.format("Distance of %d: %.2f", beacon.getMajor(), distance));
        return distance <= MAX_DISTANCE;
    }

    private void sendToPiwik(Beacon beacon) {
        /* Send beacon data and device unique ID to Piwik */

        // get minor and major IDs of the beacon
        Integer major = beacon.getMajor();
        Integer minor = beacon.getMinor();

        // calculate device UUID using bluetooth MAC address
        BluetoothAdapter blAdapter = BluetoothAdapter.getDefaultAdapter();
        String blMAC = blAdapter.getAddress();
        UUID deviceID = new UUID(blMAC.hashCode(), blMAC.hashCode());
        Log.i(TAG, "Device ID " + deviceID );

        // TODO: implement REST API call
    }
}
